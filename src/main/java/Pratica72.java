
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import utfpr.ct.dainf.if62c.pratica.ContadorPalavras;
import utfpr.ct.dainf.if62c.pratica.Palavra;
import utfpr.ct.dainf.if62c.pratica.PalavraComparador;

public class Pratica72 {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        System.out.print("Arquivo: ");
        String path1 = s.next();
        ContadorPalavras c = new ContadorPalavras(new File(path1));

        //File f = new File("/root/UDPServer.java");
        //ContadorPalavras c = new ContadorPalavras(f);
        HashMap<String, Integer> palavras = c.getPalavras();

        System.out.println("--------------");
        List<Palavra> lista = new ArrayList();
        Set<Map.Entry<String, Integer>> itens = palavras.entrySet();
        //System.out.println("Size: " + itens.size());        
        for (Map.Entry<String, Integer> item : itens) {
            System.out.println(item.getKey() + "\t" + item.getValue());
            //Exemplo: 'teste','14'
            lista.add(new Palavra(item.getValue(), item.getKey()));
        }

        System.out.println("======");
        //Ordem decrescente
        PalavraComparador j = new PalavraComparador(false);
        Collections.sort(lista, j);
        
        //Grava no segundo arquivo
        try {
            String path2 = path1 + ".out";
            BufferedWriter bw = new BufferedWriter(new FileWriter(path2));

            String linha;
            Iterator it = lista.iterator();
            while (it.hasNext()) {
                Palavra p = (Palavra) it.next();
                linha = p.getValor() + "\t" + p.getChave()+"\n";
                bw.write(linha);
                System.out.println(linha);
            }
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(Pratica72.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
