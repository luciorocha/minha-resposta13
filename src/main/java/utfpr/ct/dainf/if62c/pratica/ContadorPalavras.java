/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author root
 */
public class ContadorPalavras {

    private BufferedReader reader;

    public ContadorPalavras(File arquivo) {
        try {
            this.reader = new BufferedReader(new FileReader(arquivo));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ContadorPalavras.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public HashMap<String, Integer> getPalavras() {

        HashMap<String, Integer> map = new HashMap();

        try {
            boolean achouDelimitador = false;
            boolean achouCaracter = false;
            boolean temLetra = false;
            int i = 0;
            String palavra = "";
            char[] sim
                    = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'w', 'z', 'ç',
                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'W', 'Z', 'Ç',
                        'á', 'à', 'ã', 'â',
                        'Á', 'À', 'Ã', 'Â',
                        'é', 'ê',
                        'É', 'Ê',
                        'í',
                        'Í',
                        'ó', 'õ', 'ô',
                        'Ó', 'Õ', 'Ô',
                        'ú', 'ü',
                        'Ú', 'Ü'};
            int c = this.reader.read();
            while (c != -1) {//Tentativa de ler N palavras

                palavra = "";
                achouCaracter = false;//Existe ao menos 1 letra
                achouDelimitador = false;
                while (!achouDelimitador) { //Tentativa de ler 1 palavra
                    i = 0;                    
                    temLetra = false;
                    achouDelimitador = false;
                    while (i < sim.length && !temLetra) {
                        if ((char) c == sim[i]) {
                            achouCaracter = true;
                            temLetra = true;
                        }
                        i++;
                    }
                    if (temLetra) {                        
                        palavra += (char) c;
                        //System.out.print(palavra);
                    } else {
                        achouDelimitador = true; //Saida
                        //Guarda a palavra no mapa, mas apenas se achou 1 caracter
                        //
                        if (achouCaracter) {
                            //Se jah existe, incrementa a quantidade
                            //System.out.println("Palavra: ["+palavra+"]");
                            if (map.containsKey(palavra)) {
                                map.replace(palavra, map.get(palavra)+1);
                                //System.out.println(palavra + " - " + map.get(palavra));
                            } else {
                                //Se nao existe, adiciona 1
                                map.put(palavra, 1);
                                //System.out.println(palavra + " = " + map.get(palavra));
                            }
                        }   //Achou um simbolo
                        //else
                        //  System.out.println('Simbolo');                        
                    }//fim else
                    //Proximo caracter do arquivo
                    c = this.reader.read();
                }
            }
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(ContadorPalavras.class.getName()).log(Level.SEVERE, null, ex);
        }

        return map;
    }

}
