/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author root
 */
public class Palavra {
    
    int chave;
    String valor;
    
    public Palavra(int chave, String valor){
        this.chave=chave;
        this.valor=valor;
    }

    public int getChave() {
        return chave;
    }

    public String getValor() {
        return valor;
    }
    
    
    
}
