/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;

/**
 *
 * @author root
 */
public class PalavraComparador implements Comparator<Palavra>{

    private boolean t1;    
    
    public PalavraComparador(boolean t1){
        
        this.t1=t1;
        
    }
    
    @Override
    public int compare(Palavra o1, Palavra o2) {
        
        //Ordem crescente
        if (this.t1 == true){
        
        if (o1.chave < o2.chave){
            return -1;
        }
        if (o1.chave > o2.chave){
            return 1;
        }
        return 0;
        } else {
            //Ordem decrescente
            if (o1.chave < o2.chave){
            return 1;
        }
        if (o1.chave > o2.chave){
            return -1;
        }
        return 0;            
        }
        
    }
    
}
